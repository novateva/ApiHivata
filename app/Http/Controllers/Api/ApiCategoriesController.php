<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Event;
use App\Category;

class ApiCategoriesController extends Controller
{
    public function getHistory() {
      return Category::all();
    }

    public function getEventsByCategory(Request $request) {
      return Category::with('events')->where('id', $request->categories_id)->get();
    }

    public function getProfilesByCategory(Request $request) {
      return Category::with('profiles.user')->where('id', $request->categories_id)->get();
    }

    public function getCategory(Request $request) {
      return Category::where('id', $request->id)->get();
    }

    public function createCategory(Request $request)
    {

    	$category = new Category;

      $category->name = $request->name;

      $category->save();

      return response()->json(['status' => "success"], 200);
    }

    public function updateCategory(Request $request) {

      $category = Category::find($request->id);

      $category->name = $request->name;

      $category->save();

      return response()->json(['status' => "success"], 200);
    }

    public function deleteCategory(Request $request) {

      $category = Category::find($request->id);
      $category->delete();

      return response()->json(['status' => "success"], 200);
  }
}
