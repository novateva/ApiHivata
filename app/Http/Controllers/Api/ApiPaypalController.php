<?php

namespace App\Http\Controllers\Api;
use JWTAuth;
use App\User;
use App\paypalOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\Amount;
use PayPal\Api\ItemList;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\Payee;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Input;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Agreement;

 
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

class ApiPaypalController extends Controller
{
    public function __construct()
    {
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    public function create(Request $request)
    {
        try {
            $user = JWTAuth::parseToken()->toUser();
            $user = User::find($user->id);
       	} catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }
        
        $code = $request->code;
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $item_1->setName('Pago de evento') /** item name **/
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($request->buget); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($request->buget);

        $payee = new Payee();
        $userPayee = User::find($request->iduser);
        $payee->setEmail($userPayee->email);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setPayee($payee)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(url('/').'/api/paypal-update-status?status=1&code='.$code) /** Specify return URL **/
            ->setCancelUrl(url('/').'/api/paypal-update-status?status=1&code='.$code);

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            return 'error creando pago';
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        //Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
        /** redirect to paypal **/
            //$order = new paypalOrder;
            //$order->user_id = $user->id;
            //$order->user_id_to = $userPayee->id;
            //$order->order_id = $payment->getId();
            //$order->save();
 
            return ['url'=>$redirect_url, 'id'=>$payment->getId() ];
        }
        
        return 'error en el redirect';
    }
    
    public function updateDataUser($id, $value)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/hivata-87305-dff57504bc0e.json');
 
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://hivata-87305.firebaseio.com/')
        ->create();
        
        $database = $firebase->getDatabase();
        $newPost = $database
        ->getReference("updateuser/{$id}/payment")
        ->set([
            'value' => $value
        ]);
    }

    public function updateStatus()
    {
        $code = Input::get('code');
        if (Input::get('status') == '1') {
            $payment_id = Input::get('paymentId');
            if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
                $this->updateDataUser($code,false);
                return ['status'=>'canceled'];
            }
            $payment = Payment::get($payment_id, $this->_api_context);
            $execution = new PaymentExecution();
            $execution->setPayerId(Input::get('PayerID'));
            /**Execute the payment **/
            $result = $payment->execute($execution, $this->_api_context);
            if ($result->getState() == 'approved') {
                $this->updateDataUser($code,true);
                return ['status'=>'pagado'];
            }
            $this->updateDataUser($code,false);
            return ['status'=>'canceled'];
        } else {
            if (isset($_GET['success']) && $_GET['success'] == 'true') {
                $token = $_GET['token'];
                $agreement = new \PayPal\Api\Agreement();
                
                try {
                    // Execute agreement
                    $agreement = $agreement->execute($token, $this->_api_context);
                    $this->updateDataUser($code,true);
                    return ['status'=>'pagado'];
                } catch (PayPal\Exception\PayPalConnectionException $ex) {

                    $this->updateDataUser($code,false);
                    return ['status'=>'canceled'];
                    die($ex);
                } catch (Exception $ex) {
                    $this->updateDataUser($code,false);
                    return ['status'=>'canceled'];
                    die($ex);
                }
            } else {
                $this->updateDataUser($code,false);
                return ['status'=>'canceled'];
            }
        }
         
    }

    public function makePlan(Request $request)
    {
        $plan = new Plan();
        $plan->setName('Hivata suscription premium mode')
        ->setDescription('Make everting happend')
        ->setType('fixed');

        // Set billing plan definitions
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
        ->setType('REGULAR')
        ->setFrequency('Month')
        ->setFrequencyInterval('1')
        ->setCycles('12')
        ->setAmount(new Currency(array('value' => 1, 'currency' => 'USD')));

        // Set charge models
        $chargeModel = new ChargeModel();
        $chargeModel->setType('SHIPPING')
        ->setAmount(new Currency(array('value' => 1, 'currency' => 'USD')));
        $paymentDefinition->setChargeModels(array($chargeModel));

        // Set merchant preferences
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl(url('/').'/api/paypal-update-status?status=2&success=true')
        ->setCancelUrl(url('/').'/api/paypal-update-status?status=2')
        //->setNotifyUrl(url('/').'/paypal-update-status')
        ->setAutoBillAmount('yes')
        ->setInitialFailAmountAction('CONTINUE')
        ->setMaxFailAttempts('0')
        ->setSetupFee(new Currency(array('value' => 1, 'currency' => 'USD')));

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);
        try {
            $createdPlan = $plan->create($this->_api_context);
          
            try {
              $patch = new Patch();
              $value = new PayPalModel('{"state":"ACTIVE"}');
              $patch->setOp('replace')
                ->setPath('/')
                ->setValue($value);
              $patchRequest = new PatchRequest();
              $patchRequest->addPatch($patch);
              $createdPlan->update($patchRequest, $this->_api_context);
              $plan = Plan::get($createdPlan->getId(), $this->_api_context);
          
              // Output plan id
              return $plan->getId();
            } catch (PayPal\Exception\PayPalConnectionException $ex) {
              return $ex->getCode();
              return $ex->getData();
              die($ex);
            } catch (Exception $ex) {
              die($ex);
            }
          } catch (PayPal\Exception\PayPalConnectionException $ex) {
            return $ex->getCode();
            return $ex->getData();
            die($ex);
          } catch (Exception $ex) {
            die($ex);
          }
    }

    public function suscriptionPaypal()
    {
        $code = Input::get('code');
        $plan = new Plan();
        $plan->setName('Hivata suscription premium mode')
        ->setDescription('Make everting happend')
        ->setType('fixed');

        // Set billing plan definitions
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
        ->setType('REGULAR')
        ->setFrequency('Month')
        ->setFrequencyInterval('1')
        ->setCycles('12')
        ->setAmount(new Currency(array('value' => 1, 'currency' => 'USD')));

        // Set charge models
        $chargeModel = new ChargeModel();
        $chargeModel->setType('SHIPPING')
        ->setAmount(new Currency(array('value' => 1, 'currency' => 'USD')));
        $paymentDefinition->setChargeModels(array($chargeModel));

        // Set merchant preferences
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl(url('/').'/api/paypal-update-status?status=2&success=true&code='.$code)
        ->setCancelUrl(url('/').'/api/paypal-update-status?status=2&code='.$code)
        //->setNotifyUrl(url('/').'/paypal-update-status')
        ->setAutoBillAmount('yes')
        ->setInitialFailAmountAction('CONTINUE')
        ->setMaxFailAttempts('0')
        ->setSetupFee(new Currency(array('value' => 1, 'currency' => 'USD')));

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);
        try {
        $createdPlan = $plan->create($this->_api_context);
        
        try {
            $patch = new Patch();
            $value = new PayPalModel('{"state":"ACTIVE"}');
            $patch->setOp('replace')
            ->setPath('/')
            ->setValue($value);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);
            $createdPlan->update($patchRequest, $this->_api_context);
            $plan = Plan::get($createdPlan->getId(), $this->_api_context);

            $agreement = new Agreement();
            $agreement->setName('Suscripcion a hivata premium')
            ->setDescription('Estas por disfrutar lo mejor de hivata')
            ->setStartDate('2019-06-17T9:45:04Z');

            // Set plan 
            $planAgreement = new Plan();
            $planAgreement->setId($plan->getId());
            $agreement->setPlan($planAgreement);

            // Add payer type
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');
            $agreement->setPayer($payer);

            // Adding shipping details
            
            try {
                // Create agreement
                $agreement->create($this->_api_context);
                
                
                $approvalUrl = $agreement->getApprovalLink();
                return ['url'=> $approvalUrl, 'id'=> $plan->getId()];
            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                return $ex->getCode();
                return $ex->getData();
                die($ex);
            } catch (Exception $ex) {
                die($ex);
            }
            // Output plan id
            
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            return $ex->getCode();
            return $ex->getData();
            die($ex);
        } catch (Exception $ex) {
            die($ex);
        }
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
        return $ex->getCode();
        return $ex->getData();
        die($ex);
        } catch (Exception $ex) {
        die($ex);
        }
        
    }
}
