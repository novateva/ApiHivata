<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\User;
use App\Event;
use App\Proposal;
class ApiEventsController extends Controller
{

  public function cleanArray($array, $j)
  {
    $newArray = [];
    for ($i=0; $i < count($array); $i++) { 
      if ( $j != $i) {
        array_push($newArray, $array[$i]);
      }
    }
    return $newArray;
  }
    public function getHistory() {
      try {
          $user = JWTAuth::parseToken()->toUser();
      } catch (Exception $e) {
          return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
      }
      $events = Event::with('categories')->where('status', '<', 2)->withCount('proposals')->get(); 
      
      for ($i=0; $i < count($events); $i++) { 
      
        $proposal = Proposal::where('events_id', $events[$i]->id)->where('status', '<', 2)->where('users_id', $user->id)->first();

        if ($proposal != null) {
          $events[$i]->send = true;
          if ($events[$i]->status == 1 && $proposal->status == 0) {
            $events = $this->cleanArray($events, $i);

            $i = -1;
            
          }
        } else if ($events[$i]->status == 1) {
            $events = $this->cleanArray($events, $i);
            $i = -1;
          }

      }
      return $events;
    }

    public function getUserEvents(Request $request) {
      try {
            $user = JWTAuth::parseToken()->toUser();
       	} catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
       	}
      return Event::with('categories')->withCount('proposals')->where('users_id', $user->id)->where('status', '<', 2)->get();
    }

    public function getEvent(Request $request) {
      return Event::with('categories')->withCount('proposals')->find($request->id);
    }

    public function history(Request $request)
    {
     try {
          $user = JWTAuth::parseToken()->toUser();
      } catch (Exception $e) {
          return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
      }
      return Event::with('proposals.users.profile')->where('users_id', $user->id)->where('status', '>', 1)->get();
    }
    public function create(Request $request)
    {
    	try {
            $user = JWTAuth::parseToken()->toUser();
       	} catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
       	}
    	$event = new Event;
      $event->description = $request->description;
      $event->city = $request->city;
      $event->name = $request->name;
      $event->location = $request->location;
      $event->date = $request->date;
      $event->latitude = $request->latitude;
      $event->longitude = $request->longitude;
      $event->currencies_id = $request->currencies_id;
      $event->users_id = $user->id;

      $event->save();

      $event->categories()->attach($request->categories_id);

      return response()->json(['status' => "success"], 200);
    }

    public function updateEvent(Request $request, $id) {
      $event = Event::find($id);
      try {
          $user = JWTAuth::parseToken()->toUser();
      } catch (Exception $e) {
          return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
      }
      $event->description = $request->description;
      $event->city = $request->city;
      $event->name = $request->name;
      $event->location = $request->location;
      $event->date = $request->date;
      $event->latitude = $request->latitude;
      $event->longitude = $request->longitude;
      $event->currencies_id = $request->currencies_id;
      $event->users_id = $user->id;

      $event->save();

      $event->categories()->attach($request->categories_id);

      return response()->json(['status' => "success"], 200);
    }

    public function deleteEvent($id) {
      $event = Event::find($id);
      Proposal::where('events_id', $id)->delete();
      $event->delete();

      return response()->json(['status' => "success"], 200);
  }
}
