<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function inform()
    {
    	  
        return ["Months" => User::select(
    		DB::raw("COUNT(*) as quantity"),
    		DB::raw("DATE_FORMAT(created_at,'%M %Y') as months")	
    	)->groupBy('months')->limit(5)
            ->get(),
            "Roles" => User::select(
    		DB::raw("COUNT(*) as quantity"),
    		"roles_id"	
    	)->groupBy('roles_id')
            ->get()];
    }
}
