<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaypalOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypal-order', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('users_id')->default(null);
            $table->unsignedInteger('users_id_to')->default(null);
            $table->string('status')->default('No pagado');
            $table->string('type')->default('1');
            $table->string('order_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypal-order');
    }
}
