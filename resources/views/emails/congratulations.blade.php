<!DOCTYPE html>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Felicitaciones</title>
  <link rel="stylesheet" type="text/css" href="/css/app.css" />
</head>

<body>
    <div id="container">
      <div id="header">
        
      </div>

      <div class="section sectiontop">

        <h1>Felicitaciones!</h1>

        <h2>Tu correo {{$email}} </h2>
	       <h2>ha sido validado con éxito</h2>

      </div>

    </div>
</body>
</html>