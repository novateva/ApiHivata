<?php

use Illuminate\Database\Seeder;

class profile extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'id' => 1,
            'name' => 'Comprador',
            'description' => '',
            'rating' => 3,
            'user_id' => 1, 
        ]);
        DB::table('profiles')->insert([
            'id' => 2,
            'name' => 'Cliente',
            'description' => '',
            'rating' => 0,
            'user_id' => 2, 
        ]);
        DB::table('profiles')->insert([
            'id' => 3,
            'name' => 'Proveedor',
            'description' => '',
            'rating' => 0,
            'user_id' => 3, 
        ]);
        DB::table('profiles')->insert([
            'id' => 4,
            'name' => 'Developer',
            'description' => '',
            'rating' => 0,
            'user_id' => 4, 
        ]);
    }
}
