<?php

use Illuminate\Database\Seeder;

class category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('categories')->insert([
            'id' => 1,
            'name' => 'Mobiliario',
            'iconweb' => 'fas fa-utensils',
            'iconapp' => 'md-cube',
            'iconmap' => 'https://icon-icons.com/icons2/1151/PNG/32/1486505264-food-fork-kitchen-knife-meanns-restaurant_81404.png'
        ]);

        DB::table('categories')->insert([
            'id' => 2,
            'name' => 'Animadores',
            'iconweb' => 'fas fa-film',
            'iconapp' => 'md-contacts',
            'iconmap' => 'https://icon-icons.com/icons2/1149/PNG/32/1486504374-clip-film-movie-multimedia-play-short-video_81330.png'
        ]);

        DB::table('categories')->insert([
            'id' => 3,
            'name' => 'Salones y jardines',
            'iconweb' => 'fas fa-plane',
            'iconapp' => 'md-headset',
            'iconmap' => 'https://icon-icons.com/icons2/1146/PNG/32/1486485566-airliner-rplane-flight-launch-rbus-plane_81166.png'
        ]);

        DB::table('categories')->insert([
            'id' => 4,
            'name' => 'Música en vivo',
            'iconweb' => 'fas fa-cut',
            'iconapp' => 'ios-musical-notes',
            'iconmap' => 'https://icon-icons.com/icons2/197/PNG/32/scissors_24029.png'
        ]);

        DB::table('categories')->insert([
            'id' => 5,
            'name' => 'Meseros',
            'iconweb' => 'fas fa-female ',
            'iconapp' => 'md-people',
            'iconmap' => 'https://icon-icons.com/icons2/1130/PNG/32/womaninacircle_80046.png'
        ]);

        DB::table('categories')->insert([
            'id' => 6,
            'name' => 'Banquetes',
            'iconweb' => 'fas fa-home',
            'iconapp' => 'ios-restaurant',
            'iconmap' => 'https://icon-icons.com/icons2/1151/PNG/32/1486505259-estate-home-house-building-property-real_81428.png'
        ]);

        DB::table('categories')->insert([
            'id' => 7,
            'name' => 'Fotografía',
            'iconweb' => 'fas fa-home',
            'iconapp' => 'md-camera',
            'iconmap' => 'https://icon-icons.com/icons2/1151/PNG/32/1486505259-estate-home-house-building-property-real_81428.png'
        ]);

        DB::table('categories')->insert([
            'id' => 8,
            'name' => 'Pastelerías',
            'iconweb' => 'fas fa-home',
            'iconapp' => 'ios-analytics',
            'iconmap' => 'https://icon-icons.com/icons2/1151/PNG/32/1486505259-estate-home-house-building-property-real_81428.png'
        ]);

        DB::table('categories')->insert([
            'id' => 9,
            'name' => 'Decoración y Mesas de Dulces',
            'iconweb' => 'fas fa-home',
            'iconapp' => 'md-snow',
            'iconmap' => 'https://icon-icons.com/icons2/1151/PNG/32/1486505259-estate-home-house-building-property-real_81428.png'
        ]);

        DB::table('categories')->insert([
            'id' => 10,
            'name' => 'Organizadores de Eventos',
            'iconweb' => 'fas fa-home',
            'iconapp' => 'md-man',
            'iconmap' => 'https://icon-icons.com/icons2/1151/PNG/32/1486505259-estate-home-house-building-property-real_81428.png'
        ]);
      
    }
}
