<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Profile;
use App\Category;
use Mail;
use Illuminate\Support\Facades\DB;


class ApiAuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register','recoveryPassword','confirmation','SuccesValidaton']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
             return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }

        $user = User::find($id); 
       return response()->json(['profile'=> Profile::with('categories')->where('user_id', $user->id)->first(), 'user'=> $user]);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }
        $user = User::find($user->id);
        if($request->has('email'))
            $user->email = $request->email;
        if($request->has('payment'))
            $user->payment = $request->payment;
        if($request->has('roles_id'))
            $user->roles_id = $request->roles_id;
        if($request->has('payment'))
            $user->payment = $request->payment;
        if($request->has('tokenNotification'))
            $user->token = $request->tokenNotification;
        $user->save();
        
        $profile = Profile::where('user_id', $user->id)->first();
        if($request->has('name'))
            $profile->name = $request->name;
        if($request->has('city'))
            $profile->city = $request->city;
        if($request->has('description'))
            $profile->description = $request->description;
        if($request->has('website'))
            $profile->website = $request->website;
        if($request->has('contactEmail'))
            $profile->contactEmail = $request->contactEmail;
        if($request->has('phone'))
            $profile->phone = $request->phone;
        if($request->has('cellPhone'))
            $profile->cellPhone = $request->cellPhone;
        if($request->has('genre'))
            $profile->genre = $request->genre;
        if($request->has('contactName'))
            $profile->contactName = $request->contactName;
        

        $profile->save();
        
        
        if (is_array($request->categoriasSelected) || is_object($request->categoriasSelected))
            DB::delete('DELETE From categories_profiles WHERE profiles_id =' . $profile->id);
            foreach ($request->categoriasSelected as $key => $value) {
                $profile->categories()->attach($value);
            }
        


        $token = JWTAuth::fromUser($user);
        return $this->respondWithToken($token, $user);
    }
    
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Update(Request $request)
    {
        
        try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }
        $user->email = $request->email;
        $user->token = $request->token;
        $user->payment = $request->payment;
        
        $user->roles_id = $request->roles_id;
        $user->payment = $request->payment;
        $user->save();
        
        $profile = Profile::where('user_id', $user->id);
        $profile->name = $request->name;
        $profile->description = $request->description;
        $profile->rating = $request->rating;

        $user->Profile()->save($profile);

        $profile->categories()->attach($request->categories_id);


        $token = JWTAuth::fromUser($user);
        return $this->respondWithToken($token, $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function authenticate(Request $request)
    {
        $user = User::select('*')->where('email', '=', $request->email)->first();

        $credentials = $request->only('email', 'password');

        try
        {
            $token = JWTAuth::attempt($credentials);

            if(!$token)
            {
                return response()->json(['error' => 'Datos incorrectos'], 401);
            }
        }
        catch(JWTException $e)
        {
            return response()->json(['error' => 'Ocurrio un error'], 500);
        }

        return response()->json(['token' => $token], 200);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = JWTAuth::attempt($credentials)) {
            return $this->respondWithToken($token, null);
        }

        return response()->json(['error' => 'No Autorizado'], 401);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Sesion Finalizada con exito']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh(), null);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $user)
    {
        return response()->json([
            'user' => $user,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {

       try {
            $user = JWTAuth::parseToken()->toUser();
       } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
       }


       return response()->json(['profile'=> Profile::with('categories')->where('user_id', $user->id)->first(), 'user'=> $user]);
    }

    public function register(Request $request)
    {

        $user = new User;
        $user->email = $request->email;
        $user->token = $request->token;
        $user->payment = $request->payment;
        $user->password = bcrypt($request->password);
        $user->roles_id = $request->roles_id;


        try {
            $user->save();
        } catch (Exception $e) {
           return Response::json(['error' => 'User already exists.'], HttpResponse::HTTP_CONFLICT);
        }

        $profile = new Profile;

        $profile->name = $request->name;
        $profile->city = $request->city;
        $profile->genre = $request->genre;
        $profile->description = $request->description;
        $user->payment = $request->payment;

        $profile->rating = 0;

        $user->Profile()->save($profile);

        $profile->categories()->attach($request->categories_id);

        $this->Confirmation($user->email, $request->redirectUrl, $user->id);
        $token = JWTAuth::fromUser($user);
        return $this->respondWithToken($token, $user);
    }

    public function payment(Request $request) {
        try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }
      $user = User::where('id', $user->id)->first();
      $user->payment = $request->payment;
      $user->save();

      return response()->json(['status' => "success"], 200);
    }

    public function SuccesValidaton($id)
    {
        $user = User::find($id);
        $email = $user->email;
        $user->validation = true;
        $user->save();
        return view('emails.congratulations',compact('email'));
    }

    public function finish(Request $request, $id) {
       
      $profile = new Profile;
      $profile->rating = $profile->rating + $request->rating;
      $profile->finishWork = $profile->finishWork + 1;
      $profile->save();


      return response()->json(['status' => "success", 'profile'=>$profile], 202);
    }
    public function SendPost(Request $request)
    {   
        try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
         
        }
        Mail::send('emails.contact',$request->all(), function ($msj)
        {
            $msj->subject('Msj contacto hivata');
            $msj->to('atencion_clientes@hivata.com.mx');
        });
        return ['status'=>'success'];
       
    }

    public function recoveryPassword(Request $request)
    {   
        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            return ['status'=>'success', 'user'=>$user];
        } else {
            return ['status'=>'failure'];
        }
        
       
    }

    public function Confirmation($email, $url, $id)
    {
        
        Mail::send('emails.Confirmation',['url'=>$url,'id'=>$id], function ($msj) use($email)
        {
            $msj->subject('Confirmación correo electrónico hivata');
            $msj->to($email);
        });
    }

    
}
