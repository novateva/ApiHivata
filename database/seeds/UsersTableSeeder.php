<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret'),
            'roles_id' => 1,
            'token'=> '',
            'payment'=> 1,
            'images' => 2,
            'validation' =>1
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'email' => 'cliente@test.com',
            'password' => bcrypt('123456'),
            'roles_id' => 2,
            'token'=> '',
            'payment'=> 1,
            'images' => 1,
            'validation' =>1
        ]);
        DB::table('users')->insert([
            'id' => 3,
            'email' => 'proveedor@test.com',
            'password' => bcrypt('123456'),
            'roles_id' => 1,
            'token'=> '',
            'payment'=> 1,
            'images' => 1,
            'validation' =>1
        ]);

        DB::table('users')->insert([
            'id' => 4,
            'email' => 'rojasv907@gmail.com',
            'password' => bcrypt('123456'),
            'roles_id' => 1,
            'token'=> '',
            'payment'=> 1,
            'images' => 1,
            'validation' =>1
        ]);
    }
}
