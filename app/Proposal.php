<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
     /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'proposals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'budget', 'users_id', 'status', 'currencies_id', 'events_id'];

    public function events() {
      return $this->belongsTo('App\Event', 'events_id');
    }

    public function currencies() {
      return $this->belongsTo('App\Currency', 'currencies_id');
    }

    public function users() {
      return $this->belongsTo('App\User', 'users_id');
    }
}
