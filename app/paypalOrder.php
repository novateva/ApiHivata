<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paypalOrder extends Model
{
    protected $table = 'paypal-order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['users_id', 'users_id_to','status','type','order_id'];
}
