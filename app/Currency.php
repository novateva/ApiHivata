<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
     /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'currencies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function events() {
        return $this->hasMany('App\Event', 'currencies_id');
    }

    public function proposals() {
        return $this->hasMany('App\Proposal', 'currencies_id');
    }
}
