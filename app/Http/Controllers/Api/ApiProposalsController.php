<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\User;
use App\Event;
use App\Proposal;
use App\Profile;

class ApiProposalsController extends Controller
{
    public function getHistory() {
      try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }
      return Proposal::with(['events.categories', 'users.profile'])->where('status','>', 1)->where('users_id', $user->id)->get();
    }

    public function getUserProposals(Request $request) {
      try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }
      return Proposal::with(['events.categories', 'users.profile'])->where('users_id', $user->id)->get();
    }

    public function getProposal(Request $request) {
      return Proposal::with(['events.categories', 'users.profile'])->where('events_id', $request->event_id)->where('users_id', $request->user_id)->get();
    }

    public function updateProposalStatus(Request $request) {
      
      if($request->status == 2) {
          $profile = Profile::where('user_id', $request->user_id)->first();
          $profile->rating = $profile->rating + $request->rating;
          $profile->finishWork = $profile->finishWork + 1;
          $profile->save();
      } 
      
      $event = Event::where('id',$request->event_id)->update(['status' => $request->status]);
      $proposal = Proposal::where('events_id', $request->event_id)->where('users_id', $request->user_id)->update(['status' => $request->status]);

      return response()->json(['status' => "success"], 200);
    }

    public function getEventProposals(Request $request, $id) {
      return Proposal::with('users.profile')->where('events_id', $id)->get();
    }

    public function createProposal(Request $request)
    {
      try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }

      $proposal = new Proposal;

      $proposal->description = $request->description;
      $proposal->budget = $request->budget;
      $proposal->status = $request->status;
      $proposal->events_id = $request->events_id;
      $proposal->currencies_id = $request->currencies_id;
      $proposal->users_id = $user->id;
      $proposal->save();

      return response()->json(['status' => "success"], 200);
    }

    public function updateProposal(Request $request, $id) {
      try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }

      $proposal = Proposal::where('events_id', $id)->where('users_id', $user->id)->update(['description' => $request->description,
        'budget' => $request->budget,
        'status' => $request->status,
        'currencies_id' => $request->currencies_id]);

      return response()->json(['status' => "success"], 200);
    }

    public function deleteProposal(Request $request, $id) {

      try {
            $user = JWTAuth::parseToken()->toUser();
        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }

      $proposal = Proposal::where('events_id', $id)->where('users_id', $user->id)->delete();

      return response()->json(['status' => "success"], 200);
  }

    public function notifications(Request $request)
    {
      try {
          $user = JWTAuth::parseToken()->toUser();
      } catch (Exception $e) {
          return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
      }
      $userToSend = User::find($request->iduser);
      
      $fields = array(
          'to' => $userToSend->token,
          'title' => $request->title,
          'body' => $request->body,
          'data' => array("events_id" =>$request->events_id, 'userToSend'=> $request->iduser, 'userHowSend'=> $user->id),
          
      );

      $fields = json_encode($fields);
      
      print($fields);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://exp.host/--/api/v2/push/send");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Accept: application/json',
          'Accept-Encoding: gzip, deflate',
          'Content-Type: application/json'
      ));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

      $response = curl_exec($ch);
      curl_close($ch);
    }
}
