<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Image;

class ApiImagesController extends Controller
{
    public function uploadImage(Request $request) {
      $user = User::find(request('user_id'));
      $image = new Image;

      if ($user->images >= 5) {
        return response()->json(['error' => 'No more images allowed.']);
      }

      $path = $request->file('image')->store(NULL, 'public');

      $user->images = $user->images + 1;
      $user->save();

      $image->path = $path;
      $user->images()->save($image);

      return response()->json(['status' => 'success', 'image' => $image], 200);
    }

    public function PerfilImage(Request $request)
    {
        $user = User::find(request('user_id'));

        $path = $request->file('image')->store(NULL, 'public');
        if ($user->imagenPath != '') {
            Storage::disk('public')->delete($user->imagenPath);
        }

        $user->imagenPath = $path;
        $user->save();

        return ['path' => $path, 'user'=>$user];
    }

    public function All($id) {
      return Image::where('users_id', $id)->get();
    }

    public function updateImage(Request $request) {
      $image = Image::find(request('image_id'));

      if ($path = $request->file('image')->store(NULL, 'public')) {
        Storage::disk('public')->delete($image->path);
      }

      $image->path = $path;
      $image->save();

      return response()->json(['status' => 'success', 'image'=> $image], 200);
    }

    public function deleteImage(Request $request) {
      $user = User::find(request('user_id'));
      $image = Image::find(request('image_id'));

      if (Storage::disk('public')->exists($image->path)) {
        Storage::disk('public')->delete($image->path);
        $image->delete();

        $user->images--;
        $user->save();

        return response()->json(['status' => 'success'], 200);
      } else {
        if ($image != null) {
          $image->delete();
          $user->images--;
          $user->save();
        }
        return response()->json(['error' => 'Requested content not found.']);
      }
    }

    public function retrieveImage($path) {
      if (Storage::disk('public')->exists($path)) {
        $prefix_path = Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix($path);

        return response()->file($prefix_path);

      } else {
        return response()->json(['error' => 'Archivo no existe.']);
      }
    }
}
