<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'rating', 'website', 'description', 'finishWork', 'user_id', 'contactName','contactEmail', 'phone', 'cellPhone'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function categories() {
        return $this->belongsToMany('App\Category', 'categories_profiles', 'profiles_id', 'categories_id');
    }
}
