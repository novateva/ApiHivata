<?php

use Illuminate\Database\Seeder;

class currency extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'id' => 1,
            'name' => 'USD',
        ]);
        DB::table('currencies')->insert([
            'id' => 2,
            'name' => 'COP',
        ]);
    }
}
