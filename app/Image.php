<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

  protected $fillable = [
       'path', 'users_id'
  ];

  public function users() {
      return $this->belongsTo('App\Users', 'users_id');
  }
}
