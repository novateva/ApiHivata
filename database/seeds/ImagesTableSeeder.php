<?php

use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('images')->insert([
          'id' => 1,
          'users_id' => 1,
          'path' => 'eGylhXuJQYtejhtbk8QdBcwomc1bCwEar07H9UhF.jpeg',
          'created_at' => '2018-08-08 14:23:30',
          'updated_at' => '2018-08-08 14:23:30',
      ]);

      DB::table('images')->insert([
          'id' => 2,
          'users_id' => 1,
          'path' => 'i3U2wsTDnuKW2rxkUo0tGt1XtuYLVyeyj6BV0wmo.jpeg',
          'created_at' => '2018-08-08 14:23:30',
          'updated_at' => '2018-08-08 14:23:30',
      ]);

      DB::table('images')->insert([
          'id' => 3,
          'users_id' => 2,
          'path' => 'zYIOqCaeXTVEkOvxXbVDRfxhJuRSHO5GaOF43wfJ.jpeg',
          'created_at' => '2018-08-08 14:23:30',
          'updated_at' => '2018-08-08 14:23:30',
      ]);
    }
}
