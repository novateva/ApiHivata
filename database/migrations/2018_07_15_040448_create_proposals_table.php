<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->integer('status');
            $table->float('budget',8 ,2);
            $table->text('description');
            $table->integer('users_id')->unsigned();            
            $table->foreign('users_id')->references('id')->on('users'); 
            $table->integer('events_id')->unsigned();            
            $table->foreign('events_id')->references('id')->on('events');
            $table->integer('currencies_id')->unsigned();            
            $table->foreign('currencies_id')->references('id')->on('currencies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
