<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_profiles', function (Blueprint $table) {
            
            $table->integer('profiles_id')->unsigned();            
            $table->foreign('profiles_id')->references('id')->on('profiles'); 
            $table->integer('categories_id')->unsigned();            
            $table->foreign('categories_id')->references('id')->on('categories');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_profiles');
    }
}
