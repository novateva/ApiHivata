<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api'

], function ($router) {
    Auth::routes();
    Route::post('authenticate', 'Api\ApiAuthController@authenticate');
    Route::post('register', 'Api\ApiAuthController@register');
    Route::post('login', 'Api\ApiAuthController@login');
    Route::post('logout', 'Api\ApiAuthController@logout');
    Route::post('refresh', 'Api\ApiAuthController@refresh');
    Route::get('me', 'Api\ApiAuthController@me');
    Route::get('auth/facebook', 'Api\ApiFacebookController@redirectToProvider');
    Route::post('auth/facebook/callback', 'Api\ApiFacebookController@handleProviderCallback');
    Route::get('auth/google', 'Api\ApiGoogleController@redirectToProvider');
    Route::post('auth/google/callback', 'Api\ApiGoogleController@handleProviderCallback');
    Route::get('/restricted', 'Api\ApiAuthController@me');
    Route::post('/updatePayment','Api\ApiAuthController@payment');
    Route::post('/finishWork/{id}', 'Api\ApiAuthController@finish');
    Route::post('/sendpost', 'Api\ApiAuthController@SendPost');
    Route::get('/user/email', 'Api\ApiAuthController@recoveryPassword');
    Route::get('/confirmation/{email}', 'Api\ApiAuthController@confirmation');



    Route::put('me', 'Api\ApiAuthController@edit');
    Route::get('user/{id}', 'Api\ApiAuthController@show');
    //Event routes
    Route::post('/new-event', 'Api\ApiEventsController@create' );
    Route::get('/get-events', 'Api\ApiEventsController@getHistory' );
    Route::get('/my-history', 'Api\ApiEventsController@history' );
    Route::get('/my-events', 'Api\ApiEventsController@getUserEvents' );
    Route::get('/event', 'Api\ApiEventsController@getEvent' );
    Route::put('/update-event/{id}', 'Api\ApiEventsController@updateEvent' );
    Route::delete('/delete-event/{id}', 'Api\ApiEventsController@deleteEvent' );

    //Proposal routes
    Route::post('/new-proposal', 'Api\ApiProposalsController@createProposal' );
    Route::get('/get-proposals', 'Api\ApiProposalsController@getHistory' );
    Route::get('/my-proposals', 'Api\ApiProposalsController@getUserProposals' );
    Route::get('/proposal', 'Api\ApiProposalsController@getProposal' );
    Route::get('/event/{id}/proposals', 'Api\ApiProposalsController@getEventProposals' );
    Route::put('/update-proposal/event/{id}', 'Api\ApiProposalsController@updateProposal' );
    Route::put('/update-proposal-status', 'Api\ApiProposalsController@updateProposalStatus' );
    Route::delete('/delete-proposal/event/{id}', 'Api\ApiProposalsController@deleteProposal' );


    //Category routes
    Route::post('/new-category', 'Api\ApiCategoriesController@createCategory' );
    Route::get('/get-categories', 'Api\ApiCategoriesController@getHistory' );
    Route::get('/get-category', 'Api\ApiCategoriesController@getCategory' );
    Route::get('/get-events-by-category', 'Api\ApiCategoriesController@getEventsByCategory' );
    Route::get('/get-profiles-by-category', 'Api\ApiCategoriesController@getProfilesByCategory' );
    Route::put('/update-category', 'Api\ApiCategoriesController@updateCategory' );
    Route::delete('/delete-category', 'Api\ApiCategoriesController@deleteCategory' );

    //notifications
    Route::post('/sendNotifications', 'Api\ApiProposalsController@notifications');

    //Images Routes
    Route::post('/upload-image-perfil', 'Api\ApiImagesController@PerfilImage');
    Route::post('/upload-image', 'Api\ApiImagesController@uploadImage');
    Route::post('/delete-image', 'Api\ApiImagesController@deleteImage');
    Route::get('/retrieve-image/{path}', 'Api\ApiImagesController@retrieveImage');
    Route::post('/update-image', 'Api\ApiImagesController@updateImage');
    Route::get('/image-user/{id}','Api\ApiImagesController@All');

    //Paypal routing
    Route::post('/created-transaccion','Api\ApiPaypalController@create');
    Route::get('/suscribed','Api\ApiPaypalController@suscriptionPaypal');
    Route::get('/paypal-update-status','Api\ApiPaypalController@updateStatus');
    Route::get('/makePlan','Api\ApiPaypalController@makePlan');
    Route::get('/updatefirebase/{id}/{value}','Api\ApiPaypalController@updateDataUser');

    //Ruta correo
    Route::get('/SucessValidaton/{id}','Api\ApiAuthController@SuccesValidaton')->name('sucess');


    Route::get('/home', 'HomeController@index')->name('homeapi');

});
