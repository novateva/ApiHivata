<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->default('')->nullable();
            $table->string('website')->default('')->nullable();
            $table->string('contactEmail')->default('')->nullable();
            $table->string('phone')->default('')->nullable();
            $table->string('city')->default('')->nullable();
            $table->string('genre')->default('')->nullable();
            $table->string('cellPhone')->default('')->nullable();
            $table->string('contactName')->default('')->nullable();
            $table->integer('rating')->default(0);
            $table->integer('finishWork')->default(0);
            $table->integer('user_id')->unsigned();            
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
