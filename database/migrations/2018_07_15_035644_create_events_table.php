<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->datetime('date');
            $table->float('longitude', 8, 2);
            $table->float('latitude', 8, 2);
            $table->integer('status')->unsigned()->default(0);
            $table->string('name');
            $table->string('location');
            $table->string('city');
            $table->integer('currencies_id')->unsigned();
            $table->foreign('currencies_id')->references('id')->on('currencies');
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
